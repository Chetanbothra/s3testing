# This find the folder AWS
# provider "aws" {
#     profile = "default"
#     region = var.aws_reg
# }
provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region = var.aws_reg
}

# We will craete a s3 buckets
resource "aws_s3_bucket" "my_terraform_bucket" {
    bucket = var.bucket
    acl = "private"
}

